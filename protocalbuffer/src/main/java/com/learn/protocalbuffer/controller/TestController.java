package com.learn.protocalbuffer.controller;

import model.StudentProto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {


    @GetMapping(value = "/student", produces = "application/x-protobuf")
    StudentProto.Student student(){
        return StudentProto.Student.newBuilder().setId(2).build();
    }

    @GetMapping(value = "/students", produces = "application/x-protobuf")
    StudentProto.Students students(){

        List<StudentProto.Student> students = new ArrayList<>();
        students.add(StudentProto.Student.newBuilder().setId(1).setCustomerId(1).setNumber("111111").build());
        students.add(StudentProto.Student.newBuilder().setId(2).setCustomerId(2).setNumber("222222").build());
        students.add(StudentProto.Student.newBuilder().setId(3).setCustomerId(3).setNumber("333333").build());
        students.add(StudentProto.Student.newBuilder().setId(4).setCustomerId(4).setNumber("444444").build());
        students.add(StudentProto.Student.newBuilder().setId(5).setCustomerId(1).setNumber("555555").build());
        students.add(StudentProto.Student.newBuilder().setId(6).setCustomerId(2).setNumber("666666").build());
        students.add(StudentProto.Student.newBuilder().setId(7).setCustomerId(2).setNumber("777777").build());
    return StudentProto.Students.newBuilder().addAllAccount(students).build();
    }
}
