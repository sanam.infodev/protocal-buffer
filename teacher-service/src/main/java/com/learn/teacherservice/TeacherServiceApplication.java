package com.learn.teacherservice;

import com.learn.teacherservice.service.repo.TeacherRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.piomin.services.grpc.account.model.TeacherPorto;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class TeacherServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeacherServiceApplication.class, args);
	}

	@Bean
	public TeacherRepository repository(){
		List<TeacherPorto.Teacher> teachers = new ArrayList<>();
		teachers.add(TeacherPorto.Teacher.newBuilder().setId(1).setAddress("address1").setStudentId(1).setName("teacher1").build());
		teachers.add(TeacherPorto.Teacher.newBuilder().setId(2).setAddress("address2").setStudentId(2).setName("teacher2").build());
		teachers.add(TeacherPorto.Teacher.newBuilder().setId(3).setAddress("address3").setStudentId(2).setName("teacher3").build());
		teachers.add(TeacherPorto.Teacher.newBuilder().setId(4).setAddress("address4").setStudentId(4).setName("teacher4").build());
		teachers.add(TeacherPorto.Teacher.newBuilder().setId(5).setAddress("address5").setStudentId(5).setName("teacher5").build());
		teachers.add(TeacherPorto.Teacher.newBuilder().setId(6).setAddress("address6").setStudentId(6).setName("teacher6").build());

		return new TeacherRepository(teachers);


	}

}
