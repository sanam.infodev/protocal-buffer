package com.learn.teacherservice.service;

import com.google.protobuf.Empty;
import com.learn.teacherservice.service.repo.TeacherRepository;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import pl.piomin.services.grpc.account.model.TeacherPorto;
import pl.piomin.services.grpc.account.model.TeachersServiceGrpc;

@GrpcService
public class TeacherService extends TeachersServiceGrpc.TeachersServiceImplBase {

    @Autowired
    private TeacherRepository teacherRepository;

    @Override
    public void findAll(Empty request, StreamObserver<TeacherPorto.Teachers> responseObserver) {
        responseObserver.onNext(TeacherPorto.Teachers.newBuilder().addAllTeacher(teacherRepository.getAllTeacher()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void saveTeacher(TeacherPorto.Teacher request, StreamObserver<TeacherPorto.Teacher> responseObserver) {
         request = teacherRepository.save(request);
        responseObserver.onNext(request);
        responseObserver.onCompleted();
    }

    @Override
    public void getAllTeacherByStudentId(TeacherPorto.GetTeacherRequest request, StreamObserver<TeacherPorto.Teachers> responseObserver) {
        responseObserver.onNext(teacherRepository.getAllTeacherByStudentId(request));
        responseObserver.onCompleted();
    }
}
