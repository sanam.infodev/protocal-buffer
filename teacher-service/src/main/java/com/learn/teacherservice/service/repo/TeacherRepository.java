package com.learn.teacherservice.service.repo;

import pl.piomin.services.grpc.account.model.TeacherPorto;

import java.util.List;
import java.util.stream.Collectors;

public class TeacherRepository {

    private List<TeacherPorto.Teacher> teachers;

    public TeacherRepository(List<TeacherPorto.Teacher> teachers){
        this.teachers = teachers;
    }

    public List<TeacherPorto.Teacher> getAllTeacher(){
        return this.teachers;
    }

    public TeacherPorto.Teacher save(TeacherPorto.Teacher teacher){
       TeacherPorto.Teacher teacher1=  TeacherPorto.Teacher.newBuilder().setId(teachers.size() + 1).setName(teacher.getName()).setStudentId(teacher.getStudentId()).setAddress(teacher.getAddress()).build();
        this.teachers.add(teacher1);
        return  teacher1;
    }

    public TeacherPorto.Teachers getAllTeacherByStudentId(TeacherPorto.GetTeacherRequest getTeacherRequest){
        return TeacherPorto.Teachers.newBuilder().addAllTeacher(teachers.stream().filter( teacher -> teacher.getStudentId() == getTeacherRequest.getStudentId()).collect(Collectors.toList())).build();
    }
}
