package com.learn.teacherservice.interceptor;

import io.grpc.*;
import org.springframework.context.annotation.Configuration;

import static io.grpc.Metadata.ASCII_STRING_MARSHALLER;

@Configuration
public class AuthorizationServerInterceptor implements ServerInterceptor {
    public static final Metadata.Key<String> AUTHORIZATION_METADATA_KEY = Metadata.Key.of("Authorization", ASCII_STRING_MARSHALLER);
    public static final String BEARER_TYPE = "Bearer";
    public static final Context.Key<String> CLIENT_ID_CONTEXT_KEY = Context.key("clientId");



    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall, Metadata metadata, ServerCallHandler<ReqT, RespT> serverCallHandler) {

        String value = metadata.get(AUTHORIZATION_METADATA_KEY);

        Status status;
//        if (value == null) {
//            status = Status.UNAUTHENTICATED.withDescription("Authorization token is missing");
//        } else if (!value.startsWith(BEARER_TYPE)) {
//            status = Status.UNAUTHENTICATED.withDescription("Unknown authorization type");
//        } else {
            try {
//                String token = value.substring(Constants.BEARER_TYPE.length()).trim();
//                Jws<Claims> claims = parser.parseClaimsJws(token);
//                Context ctx = Context.current().withValue(Constants.CLIENT_ID_CONTEXT_KEY, claims.getBody().getSubject());
                Context ctx = Context.current().withValue(CLIENT_ID_CONTEXT_KEY, "sanam");
                return Contexts.interceptCall(ctx, serverCall, metadata, serverCallHandler);
            } catch (Exception e) {
                status = Status.UNAUTHENTICATED.withDescription(e.getMessage()).withCause(e);
            }
//        }

        serverCall.close(status, metadata);
        return new ServerCall.Listener<>() {
            // noop
        };    }
}
