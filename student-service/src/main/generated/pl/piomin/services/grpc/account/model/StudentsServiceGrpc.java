package pl.piomin.services.grpc.account.model;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.57.2)",
    comments = "Source: student.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class StudentsServiceGrpc {

  private StudentsServiceGrpc() {}

  public static final java.lang.String SERVICE_NAME = "model.StudentsService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      pl.piomin.services.grpc.account.model.StudentPorto.Students> getFindAllMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "FindAll",
      requestType = com.google.protobuf.Empty.class,
      responseType = pl.piomin.services.grpc.account.model.StudentPorto.Students.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      pl.piomin.services.grpc.account.model.StudentPorto.Students> getFindAllMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, pl.piomin.services.grpc.account.model.StudentPorto.Students> getFindAllMethod;
    if ((getFindAllMethod = StudentsServiceGrpc.getFindAllMethod) == null) {
      synchronized (StudentsServiceGrpc.class) {
        if ((getFindAllMethod = StudentsServiceGrpc.getFindAllMethod) == null) {
          StudentsServiceGrpc.getFindAllMethod = getFindAllMethod =
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, pl.piomin.services.grpc.account.model.StudentPorto.Students>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "FindAll"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pl.piomin.services.grpc.account.model.StudentPorto.Students.getDefaultInstance()))
              .setSchemaDescriptor(new StudentsServiceMethodDescriptorSupplier("FindAll"))
              .build();
        }
      }
    }
    return getFindAllMethod;
  }

  private static volatile io.grpc.MethodDescriptor<pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest,
      pl.piomin.services.grpc.account.model.StudentPorto.Teachers> getFindAllTeacherByStudentIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "FindAllTeacherByStudentId",
      requestType = pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest.class,
      responseType = pl.piomin.services.grpc.account.model.StudentPorto.Teachers.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest,
      pl.piomin.services.grpc.account.model.StudentPorto.Teachers> getFindAllTeacherByStudentIdMethod() {
    io.grpc.MethodDescriptor<pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest, pl.piomin.services.grpc.account.model.StudentPorto.Teachers> getFindAllTeacherByStudentIdMethod;
    if ((getFindAllTeacherByStudentIdMethod = StudentsServiceGrpc.getFindAllTeacherByStudentIdMethod) == null) {
      synchronized (StudentsServiceGrpc.class) {
        if ((getFindAllTeacherByStudentIdMethod = StudentsServiceGrpc.getFindAllTeacherByStudentIdMethod) == null) {
          StudentsServiceGrpc.getFindAllTeacherByStudentIdMethod = getFindAllTeacherByStudentIdMethod =
              io.grpc.MethodDescriptor.<pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest, pl.piomin.services.grpc.account.model.StudentPorto.Teachers>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "FindAllTeacherByStudentId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pl.piomin.services.grpc.account.model.StudentPorto.Teachers.getDefaultInstance()))
              .setSchemaDescriptor(new StudentsServiceMethodDescriptorSupplier("FindAllTeacherByStudentId"))
              .build();
        }
      }
    }
    return getFindAllTeacherByStudentIdMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static StudentsServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<StudentsServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<StudentsServiceStub>() {
        @java.lang.Override
        public StudentsServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new StudentsServiceStub(channel, callOptions);
        }
      };
    return StudentsServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static StudentsServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<StudentsServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<StudentsServiceBlockingStub>() {
        @java.lang.Override
        public StudentsServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new StudentsServiceBlockingStub(channel, callOptions);
        }
      };
    return StudentsServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static StudentsServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<StudentsServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<StudentsServiceFutureStub>() {
        @java.lang.Override
        public StudentsServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new StudentsServiceFutureStub(channel, callOptions);
        }
      };
    return StudentsServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public interface AsyncService {

    /**
     */
    default void findAll(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Students> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getFindAllMethod(), responseObserver);
    }

    /**
     */
    default void findAllTeacherByStudentId(pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest request,
        io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teachers> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getFindAllTeacherByStudentIdMethod(), responseObserver);
    }
  }

  /**
   * Base class for the server implementation of the service StudentsService.
   */
  public static abstract class StudentsServiceImplBase
      implements io.grpc.BindableService, AsyncService {

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return StudentsServiceGrpc.bindService(this);
    }
  }

  /**
   * A stub to allow clients to do asynchronous rpc calls to service StudentsService.
   */
  public static final class StudentsServiceStub
      extends io.grpc.stub.AbstractAsyncStub<StudentsServiceStub> {
    private StudentsServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StudentsServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new StudentsServiceStub(channel, callOptions);
    }

    /**
     */
    public void findAll(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Students> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getFindAllMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void findAllTeacherByStudentId(pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest request,
        io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teachers> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getFindAllTeacherByStudentIdMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * A stub to allow clients to do synchronous rpc calls to service StudentsService.
   */
  public static final class StudentsServiceBlockingStub
      extends io.grpc.stub.AbstractBlockingStub<StudentsServiceBlockingStub> {
    private StudentsServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StudentsServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new StudentsServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public pl.piomin.services.grpc.account.model.StudentPorto.Students findAll(com.google.protobuf.Empty request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getFindAllMethod(), getCallOptions(), request);
    }

    /**
     */
    public pl.piomin.services.grpc.account.model.StudentPorto.Teachers findAllTeacherByStudentId(pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getFindAllTeacherByStudentIdMethod(), getCallOptions(), request);
    }
  }

  /**
   * A stub to allow clients to do ListenableFuture-style rpc calls to service StudentsService.
   */
  public static final class StudentsServiceFutureStub
      extends io.grpc.stub.AbstractFutureStub<StudentsServiceFutureStub> {
    private StudentsServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StudentsServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new StudentsServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<pl.piomin.services.grpc.account.model.StudentPorto.Students> findAll(
        com.google.protobuf.Empty request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getFindAllMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<pl.piomin.services.grpc.account.model.StudentPorto.Teachers> findAllTeacherByStudentId(
        pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getFindAllTeacherByStudentIdMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_FIND_ALL = 0;
  private static final int METHODID_FIND_ALL_TEACHER_BY_STUDENT_ID = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AsyncService serviceImpl;
    private final int methodId;

    MethodHandlers(AsyncService serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_FIND_ALL:
          serviceImpl.findAll((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Students>) responseObserver);
          break;
        case METHODID_FIND_ALL_TEACHER_BY_STUDENT_ID:
          serviceImpl.findAllTeacherByStudentId((pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest) request,
              (io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teachers>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  public static final io.grpc.ServerServiceDefinition bindService(AsyncService service) {
    return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
        .addMethod(
          getFindAllMethod(),
          io.grpc.stub.ServerCalls.asyncUnaryCall(
            new MethodHandlers<
              com.google.protobuf.Empty,
              pl.piomin.services.grpc.account.model.StudentPorto.Students>(
                service, METHODID_FIND_ALL)))
        .addMethod(
          getFindAllTeacherByStudentIdMethod(),
          io.grpc.stub.ServerCalls.asyncUnaryCall(
            new MethodHandlers<
              pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest,
              pl.piomin.services.grpc.account.model.StudentPorto.Teachers>(
                service, METHODID_FIND_ALL_TEACHER_BY_STUDENT_ID)))
        .build();
  }

  private static abstract class StudentsServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    StudentsServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return pl.piomin.services.grpc.account.model.StudentPorto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("StudentsService");
    }
  }

  private static final class StudentsServiceFileDescriptorSupplier
      extends StudentsServiceBaseDescriptorSupplier {
    StudentsServiceFileDescriptorSupplier() {}
  }

  private static final class StudentsServiceMethodDescriptorSupplier
      extends StudentsServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final java.lang.String methodName;

    StudentsServiceMethodDescriptorSupplier(java.lang.String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (StudentsServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new StudentsServiceFileDescriptorSupplier())
              .addMethod(getFindAllMethod())
              .addMethod(getFindAllTeacherByStudentIdMethod())
              .build();
        }
      }
    }
    return result;
  }
}
