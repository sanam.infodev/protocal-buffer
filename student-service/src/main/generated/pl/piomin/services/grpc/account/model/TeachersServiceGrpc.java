package pl.piomin.services.grpc.account.model;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.57.2)",
    comments = "Source: student.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class TeachersServiceGrpc {

  private TeachersServiceGrpc() {}

  public static final java.lang.String SERVICE_NAME = "model.TeachersService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      pl.piomin.services.grpc.account.model.StudentPorto.Teachers> getFindAllMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "FindAll",
      requestType = com.google.protobuf.Empty.class,
      responseType = pl.piomin.services.grpc.account.model.StudentPorto.Teachers.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      pl.piomin.services.grpc.account.model.StudentPorto.Teachers> getFindAllMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, pl.piomin.services.grpc.account.model.StudentPorto.Teachers> getFindAllMethod;
    if ((getFindAllMethod = TeachersServiceGrpc.getFindAllMethod) == null) {
      synchronized (TeachersServiceGrpc.class) {
        if ((getFindAllMethod = TeachersServiceGrpc.getFindAllMethod) == null) {
          TeachersServiceGrpc.getFindAllMethod = getFindAllMethod =
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, pl.piomin.services.grpc.account.model.StudentPorto.Teachers>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "FindAll"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pl.piomin.services.grpc.account.model.StudentPorto.Teachers.getDefaultInstance()))
              .setSchemaDescriptor(new TeachersServiceMethodDescriptorSupplier("FindAll"))
              .build();
        }
      }
    }
    return getFindAllMethod;
  }

  private static volatile io.grpc.MethodDescriptor<pl.piomin.services.grpc.account.model.StudentPorto.Teacher,
      pl.piomin.services.grpc.account.model.StudentPorto.Teacher> getSaveTeacherMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "saveTeacher",
      requestType = pl.piomin.services.grpc.account.model.StudentPorto.Teacher.class,
      responseType = pl.piomin.services.grpc.account.model.StudentPorto.Teacher.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<pl.piomin.services.grpc.account.model.StudentPorto.Teacher,
      pl.piomin.services.grpc.account.model.StudentPorto.Teacher> getSaveTeacherMethod() {
    io.grpc.MethodDescriptor<pl.piomin.services.grpc.account.model.StudentPorto.Teacher, pl.piomin.services.grpc.account.model.StudentPorto.Teacher> getSaveTeacherMethod;
    if ((getSaveTeacherMethod = TeachersServiceGrpc.getSaveTeacherMethod) == null) {
      synchronized (TeachersServiceGrpc.class) {
        if ((getSaveTeacherMethod = TeachersServiceGrpc.getSaveTeacherMethod) == null) {
          TeachersServiceGrpc.getSaveTeacherMethod = getSaveTeacherMethod =
              io.grpc.MethodDescriptor.<pl.piomin.services.grpc.account.model.StudentPorto.Teacher, pl.piomin.services.grpc.account.model.StudentPorto.Teacher>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "saveTeacher"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pl.piomin.services.grpc.account.model.StudentPorto.Teacher.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pl.piomin.services.grpc.account.model.StudentPorto.Teacher.getDefaultInstance()))
              .setSchemaDescriptor(new TeachersServiceMethodDescriptorSupplier("saveTeacher"))
              .build();
        }
      }
    }
    return getSaveTeacherMethod;
  }

  private static volatile io.grpc.MethodDescriptor<pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest,
      pl.piomin.services.grpc.account.model.StudentPorto.Teachers> getGetAllTeacherByStudentIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getAllTeacherByStudentId",
      requestType = pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest.class,
      responseType = pl.piomin.services.grpc.account.model.StudentPorto.Teachers.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest,
      pl.piomin.services.grpc.account.model.StudentPorto.Teachers> getGetAllTeacherByStudentIdMethod() {
    io.grpc.MethodDescriptor<pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest, pl.piomin.services.grpc.account.model.StudentPorto.Teachers> getGetAllTeacherByStudentIdMethod;
    if ((getGetAllTeacherByStudentIdMethod = TeachersServiceGrpc.getGetAllTeacherByStudentIdMethod) == null) {
      synchronized (TeachersServiceGrpc.class) {
        if ((getGetAllTeacherByStudentIdMethod = TeachersServiceGrpc.getGetAllTeacherByStudentIdMethod) == null) {
          TeachersServiceGrpc.getGetAllTeacherByStudentIdMethod = getGetAllTeacherByStudentIdMethod =
              io.grpc.MethodDescriptor.<pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest, pl.piomin.services.grpc.account.model.StudentPorto.Teachers>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getAllTeacherByStudentId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pl.piomin.services.grpc.account.model.StudentPorto.Teachers.getDefaultInstance()))
              .setSchemaDescriptor(new TeachersServiceMethodDescriptorSupplier("getAllTeacherByStudentId"))
              .build();
        }
      }
    }
    return getGetAllTeacherByStudentIdMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static TeachersServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<TeachersServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<TeachersServiceStub>() {
        @java.lang.Override
        public TeachersServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new TeachersServiceStub(channel, callOptions);
        }
      };
    return TeachersServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static TeachersServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<TeachersServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<TeachersServiceBlockingStub>() {
        @java.lang.Override
        public TeachersServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new TeachersServiceBlockingStub(channel, callOptions);
        }
      };
    return TeachersServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static TeachersServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<TeachersServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<TeachersServiceFutureStub>() {
        @java.lang.Override
        public TeachersServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new TeachersServiceFutureStub(channel, callOptions);
        }
      };
    return TeachersServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public interface AsyncService {

    /**
     */
    default void findAll(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teachers> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getFindAllMethod(), responseObserver);
    }

    /**
     */
    default void saveTeacher(pl.piomin.services.grpc.account.model.StudentPorto.Teacher request,
        io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teacher> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSaveTeacherMethod(), responseObserver);
    }

    /**
     */
    default void getAllTeacherByStudentId(pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest request,
        io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teachers> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetAllTeacherByStudentIdMethod(), responseObserver);
    }
  }

  /**
   * Base class for the server implementation of the service TeachersService.
   */
  public static abstract class TeachersServiceImplBase
      implements io.grpc.BindableService, AsyncService {

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return TeachersServiceGrpc.bindService(this);
    }
  }

  /**
   * A stub to allow clients to do asynchronous rpc calls to service TeachersService.
   */
  public static final class TeachersServiceStub
      extends io.grpc.stub.AbstractAsyncStub<TeachersServiceStub> {
    private TeachersServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TeachersServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new TeachersServiceStub(channel, callOptions);
    }

    /**
     */
    public void findAll(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teachers> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getFindAllMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void saveTeacher(pl.piomin.services.grpc.account.model.StudentPorto.Teacher request,
        io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teacher> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSaveTeacherMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getAllTeacherByStudentId(pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest request,
        io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teachers> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetAllTeacherByStudentIdMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * A stub to allow clients to do synchronous rpc calls to service TeachersService.
   */
  public static final class TeachersServiceBlockingStub
      extends io.grpc.stub.AbstractBlockingStub<TeachersServiceBlockingStub> {
    private TeachersServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TeachersServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new TeachersServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public pl.piomin.services.grpc.account.model.StudentPorto.Teachers findAll(com.google.protobuf.Empty request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getFindAllMethod(), getCallOptions(), request);
    }

    /**
     */
    public pl.piomin.services.grpc.account.model.StudentPorto.Teacher saveTeacher(pl.piomin.services.grpc.account.model.StudentPorto.Teacher request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSaveTeacherMethod(), getCallOptions(), request);
    }

    /**
     */
    public pl.piomin.services.grpc.account.model.StudentPorto.Teachers getAllTeacherByStudentId(pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetAllTeacherByStudentIdMethod(), getCallOptions(), request);
    }
  }

  /**
   * A stub to allow clients to do ListenableFuture-style rpc calls to service TeachersService.
   */
  public static final class TeachersServiceFutureStub
      extends io.grpc.stub.AbstractFutureStub<TeachersServiceFutureStub> {
    private TeachersServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TeachersServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new TeachersServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<pl.piomin.services.grpc.account.model.StudentPorto.Teachers> findAll(
        com.google.protobuf.Empty request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getFindAllMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<pl.piomin.services.grpc.account.model.StudentPorto.Teacher> saveTeacher(
        pl.piomin.services.grpc.account.model.StudentPorto.Teacher request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSaveTeacherMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<pl.piomin.services.grpc.account.model.StudentPorto.Teachers> getAllTeacherByStudentId(
        pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetAllTeacherByStudentIdMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_FIND_ALL = 0;
  private static final int METHODID_SAVE_TEACHER = 1;
  private static final int METHODID_GET_ALL_TEACHER_BY_STUDENT_ID = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AsyncService serviceImpl;
    private final int methodId;

    MethodHandlers(AsyncService serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_FIND_ALL:
          serviceImpl.findAll((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teachers>) responseObserver);
          break;
        case METHODID_SAVE_TEACHER:
          serviceImpl.saveTeacher((pl.piomin.services.grpc.account.model.StudentPorto.Teacher) request,
              (io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teacher>) responseObserver);
          break;
        case METHODID_GET_ALL_TEACHER_BY_STUDENT_ID:
          serviceImpl.getAllTeacherByStudentId((pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest) request,
              (io.grpc.stub.StreamObserver<pl.piomin.services.grpc.account.model.StudentPorto.Teachers>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  public static final io.grpc.ServerServiceDefinition bindService(AsyncService service) {
    return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
        .addMethod(
          getFindAllMethod(),
          io.grpc.stub.ServerCalls.asyncUnaryCall(
            new MethodHandlers<
              com.google.protobuf.Empty,
              pl.piomin.services.grpc.account.model.StudentPorto.Teachers>(
                service, METHODID_FIND_ALL)))
        .addMethod(
          getSaveTeacherMethod(),
          io.grpc.stub.ServerCalls.asyncUnaryCall(
            new MethodHandlers<
              pl.piomin.services.grpc.account.model.StudentPorto.Teacher,
              pl.piomin.services.grpc.account.model.StudentPorto.Teacher>(
                service, METHODID_SAVE_TEACHER)))
        .addMethod(
          getGetAllTeacherByStudentIdMethod(),
          io.grpc.stub.ServerCalls.asyncUnaryCall(
            new MethodHandlers<
              pl.piomin.services.grpc.account.model.StudentPorto.GetTeacherRequest,
              pl.piomin.services.grpc.account.model.StudentPorto.Teachers>(
                service, METHODID_GET_ALL_TEACHER_BY_STUDENT_ID)))
        .build();
  }

  private static abstract class TeachersServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    TeachersServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return pl.piomin.services.grpc.account.model.StudentPorto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("TeachersService");
    }
  }

  private static final class TeachersServiceFileDescriptorSupplier
      extends TeachersServiceBaseDescriptorSupplier {
    TeachersServiceFileDescriptorSupplier() {}
  }

  private static final class TeachersServiceMethodDescriptorSupplier
      extends TeachersServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final java.lang.String methodName;

    TeachersServiceMethodDescriptorSupplier(java.lang.String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (TeachersServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new TeachersServiceFileDescriptorSupplier())
              .addMethod(getFindAllMethod())
              .addMethod(getSaveTeacherMethod())
              .addMethod(getGetAllTeacherByStudentIdMethod())
              .build();
        }
      }
    }
    return result;
  }
}
