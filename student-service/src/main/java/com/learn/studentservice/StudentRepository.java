package com.learn.studentservice;


import pl.piomin.services.grpc.account.model.StudentPorto;

import java.util.List;

public class StudentRepository {
    List<StudentPorto.Student> students;

    public StudentRepository(List<StudentPorto.Student> students){
        this.students = students;
    }

    public List<StudentPorto.Student> getAllStudents(){
        return this.students;
    }


}
