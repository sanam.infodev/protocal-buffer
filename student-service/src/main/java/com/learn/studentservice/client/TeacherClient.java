package com.learn.studentservice.client;

import net.devh.boot.grpc.client.inject.GrpcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.piomin.services.grpc.account.model.StudentPorto;
import pl.piomin.services.grpc.account.model.TeachersServiceGrpc;

@Service
public class TeacherClient {

    @GrpcClient("teacher-service")
    private TeachersServiceGrpc.TeachersServiceBlockingStub teacherClient;

    private static final Logger LOG = LoggerFactory.getLogger(TeacherClient.class);

    public StudentPorto.Teachers getTeachersByStudentId(int studentId){
        try {
            return teacherClient.getAllTeacherByStudentId(StudentPorto.GetTeacherRequest.newBuilder().setStudentId(studentId).build());
        }catch (Exception ex){
            LOG.error(ex.getMessage());
            return null;
        }
    }

}
