package com.learn.studentservice.service;

import com.google.protobuf.Empty;
import com.learn.studentservice.StudentRepository;
import com.learn.studentservice.client.TeacherClient;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import pl.piomin.services.grpc.account.model.StudentPorto;
import pl.piomin.services.grpc.account.model.StudentsServiceGrpc;

import java.util.ArrayList;
import java.util.List;

@GrpcService
public class StudentService extends StudentsServiceGrpc.StudentsServiceImplBase {

    @Autowired
    private StudentRepository studentRepository;

    private final TeacherClient teacherClient;

    public StudentService(TeacherClient teacherClient) {
        this.teacherClient = teacherClient;
    }

    @Override
    public void findAll(Empty request, StreamObserver<StudentPorto.Students> responseObserver) {
        List<StudentPorto.Student> students = new ArrayList<>();
        students.add(StudentPorto.Student.newBuilder().setId(1).build());
        responseObserver.onNext(StudentPorto.Students.newBuilder().addAllStudent(studentRepository.getAllStudents()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void findAllTeacherByStudentId(StudentPorto.GetTeacherRequest request, StreamObserver<StudentPorto.Teachers> responseObserver) {
        StudentPorto.Teachers teachers = teacherClient.getTeachersByStudentId(request.getStudentId());
        responseObserver.onNext(teachers);
        responseObserver.onCompleted();
    }
}
