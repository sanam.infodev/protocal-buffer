package com.learn.studentservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.piomin.services.grpc.account.model.StudentPorto;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class StudentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentServiceApplication.class, args);
	}

	@Bean
	public StudentRepository repository(){
		List<StudentPorto.Student> students = new ArrayList<>();
		students.add(StudentPorto.Student.newBuilder().setId(1).setAddress("balagau").setRollNo(1).build());
		students.add(StudentPorto.Student.newBuilder().setId(2).setAddress("thankot").setRollNo(2).build());
		students.add(StudentPorto.Student.newBuilder().setId(3).setAddress("balambu").setRollNo(3).build());
		students.add(StudentPorto.Student.newBuilder().setId(4).setAddress("gujhudharra").setRollNo(4).build());
		students.add(StudentPorto.Student.newBuilder().setId(5).setAddress("satungal").setRollNo(5).build());
		students.add(StudentPorto.Student.newBuilder().setId(6).setAddress("checkpost").setRollNo(6).build());
		students.add(StudentPorto.Student.newBuilder().setId(7).setAddress("kalanki").setRollNo(7).build());
		students.add(StudentPorto.Student.newBuilder().setId(8).setAddress("kalimati").setRollNo(8).build());
		return new StudentRepository(students);
	}


}
